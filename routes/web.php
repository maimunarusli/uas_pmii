<?php

use App\Http\Controllers\FileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('Home');
// });
Route::get('/',[FileController::class ,"Baru"])-> name('jagung');
Route::get('A',[FileController::class ,"Bebek"])-> name('Kacang'); 
Route::get('B',[FileController::class ,"Itik"])-> name('Anggur');
Route::get('C',[FileController::class ,"Angsa"])-> name('Melon');
Route::get('D',[FileController::class,"Kambing"])-> name('semangka');
Route::get('E',[FileController::class,"Ikan"])-> name('Kiwi');